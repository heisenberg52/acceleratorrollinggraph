/**
 * Accelerator RollingGraph (based on RollingGraph Demo)
 * This sketch makes ise of the RollingLine2DTrace object to
 * draw accelerator values (g) of along each axis (X, Y, Z).
 * It receives the accelerator values from UDP Port 6666.
 */
 
import org.gwoptics.graphics.graph2D.Graph2D;
import org.gwoptics.graphics.graph2D.traces.ILine2DEquation;
import org.gwoptics.graphics.graph2D.traces.RollingLine2DTrace;
import hypermedia.net.*;

class FloatRingBuffer {
  float []buffer;
  int addIdx, getIdx;
  FloatRingBuffer(int size) {
    
    buffer = new float[size];
    for (int i = 0; i < buffer.length; i++) {
      buffer[i] = 0;
    }
    addIdx = 0;
    
  }
  
  void add(float newValue) {
    buffer[addIdx] = newValue;
    addIdx = (addIdx + 1) % buffer.length;
  }
  
  float get() {
    int getIdx = addIdx - 1;
    if (getIdx < 0) {
      getIdx = buffer.length - 1;
    }
    return buffer[getIdx];
  }
}



class AccelData implements ILine2DEquation{
  final private int MAX_RECORDS = 100;
  private FloatRingBuffer buffer = new FloatRingBuffer(MAX_RECORDS);
  
  public void addData(float newValue) {
    buffer.add(newValue);
  }
  
  public double computePoint(double x,int pos) {
    return buffer.get();
  }    
}

AccelData xData, yData, zData;
RollingLine2DTrace xTrace, yTrace, zTrace;
Graph2D g;

UDP udp; 

void setup(){
  size(600,300);
  
  xData = new AccelData();
  yData = new AccelData();
  zData = new AccelData();
  
  xTrace = new RollingLine2DTrace(xData, 100, 0.1f);
  xTrace.setTraceColour(255, 0, 0);
  xTrace.setLineWidth(5);
  
  yTrace = new RollingLine2DTrace(yData, 100, 0.1f);
  yTrace.setTraceColour(0, 255, 0);
  yTrace.setLineWidth(5);
  
  zTrace = new RollingLine2DTrace(zData, 100, 0.1f);
  zTrace.setTraceColour(0, 0, 255);
  zTrace.setLineWidth(5);
  
  g = new Graph2D(this, 400, 200, false);
  g.setYAxisMax(30);
  g.setYAxisMin(-30);
  g.addTrace(xTrace);
  g.addTrace(yTrace);
  g.addTrace(zTrace);
  g.position.y = 50;
  g.position.x = 100;
  g.setYAxisTickSpacing(10);
  g.setXAxisMax(5f);
  
  udp = new UDP( this, 6666 );
  udp.listen (true);
  
}
 

void draw(){
  background(200);
  g.draw();
 
}


void receive( byte[] data, String ip, int port ) {
 
  data = subset(data, 0, data.length);
  String message = new String( data );
  String[] list = split(message, ',');
 
  xData.addData(Float.parseFloat(list[1]));
  yData.addData(Float.parseFloat(list[2]));
  zData.addData(Float.parseFloat(list[3]));

  println( "receive: \""+message+"\" from "+ip+" on port "+port );
  
}
